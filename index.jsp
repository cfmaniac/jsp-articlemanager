<%@ page session="false" %>
<!--- JSP INFO --->
<%@ page import="java.lang.*" %>
<%@ page import="javax.servlet.*, javax.servlet.http.*" %>
<%@ page import="java.util.ArrayList, java.util.List, java.util.Date, java.util.Calendar, java.util.Vector, java.util.Properties, java.util.Enumeration" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.lang.reflect.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
    request.setAttribute("year", sdf.format(new java.util.Date()));
    request.setAttribute("pageTitle", "Article Manager");
    request.setAttribute("version", "1.0.1");
    request.setAttribute("pageSlogan", "There be Monsters Brewing in these labs");
    request.setAttribute("", "");

//For the Labs Page:
String root="C:/Program Files/Tomcat/85/webapps/labs/ArticleManager/articles/";
java.io.File file;
java.io.File dir = new java.io.File(root);

String[] articles = dir.list();
%>
<!DOCTYPE html>
<html>
    <head>
        <title>${pageTitle} -v.${version}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <!-- Bootstrap -->
        <link rel="stylesheet" media="screen" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" media="screen" href="assets/css/bootstrap-theme.min.css">

      <!-- Bootstrap Admin Theme -->
        <link rel="stylesheet" media="screen" href="assets/css/bootstrap-admin-theme.css">
        <link rel="stylesheet" media="screen" href="assets/css/bootstrap-admin-theme-change-size.css">

      <!-- Vendors -->
        <link rel="stylesheet" media="screen" href="assets/vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/stylesheets/bootstrap-wysihtml5/core-b3.css">

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script type="text/javascript" src="assets/js/html5shiv.js"></script>
           <script type="text/javascript" src="assets/js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="bootstrap-admin-with-small-navbar">
        <!-- small navbar -->
        <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar-sm" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left bootstrap-admin-theme-change-size">

                            </ul>
                            <ul class="nav navbar-nav navbar-right">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <!-- main / large navbar -->
        <nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar bootstrap-admin-navbar-under-small" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.jsp"></a>
                        </div>
                        <div class="collapse navbar-collapse main-navbar-collapse pull-right">
                            <ul class="nav navbar-nav">
                            Articles:
                             <select Name="Articles" id="Articles" class="form-control">
			                    <option value="">Select an Article to Read</option>
                                <%
                                    if (articles.length > 0) {

                                        for (int i = 0; i < articles.length; i++) {
                                            file = new java.io.File(root + articles[i]);
                                            if (!file.isDirectory()) {
                                %>
                                    <option value="articles/<%=articles[i]%>"><%=articles[i]%></option>
                                <%
                                            }
                                        }
                                    }
                                %>
                             </select>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div><!-- /.container -->
        </nav>

        <div class="container">
            <!-- left, vertical navbar & content -->
            <div class="row">
                <!-- left, vertical navbar -->
                <div class="col-md-2 bootstrap-admin-col-left">


                </div>

              <!-- content -->
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1>Article Manager</h1>
                            </div>
                        </div>
                    </div>



                    <div class="row">


                        <div class="col-lg-12">
                        <form name="articleman" method="POST" action="">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="text-muted bootstrap-admin-box-title"></div>
                                </div>
                                <div class="bootstrap-admin-panel-content">
                                    <strong>Title:</strong><input type="text" name="title" id="title" class="form-control">

                                    <textarea name="content" id="ckeditor_full"></textarea>

                                    <input type="submit" class="btn btn-success" value="Save">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <!-- footer -->
        <div class="navbar navbar-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <footer role="contentinfo">
                            <p class="left">${pageTitle}</p>
                            <p class="right">&copy; ${Year}</p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap-admin-theme-change-size.js"></script>

        <script type="text/javascript" src="assets/vendors/ckeditor/ckeditor.js"></script>

        <script type="text/javascript" src="assets/vendors/ckeditor/adapters/jquery.js"></script>

        <script type="text/javascript" src="assets/vendors/tinymce/js/tinymce/tinymce.min.js"></script>

        <script type="text/javascript" src="assets/vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/javascripts/bootstrap-wysihtml5/wysihtml5.js"></script>
        <script type="text/javascript" src="assets/vendors/bootstrap-wysihtml5-rails-b3/vendor/assets/javascripts/bootstrap-wysihtml5/core-b3.js"></script>

        <script type="text/javascript">
            $(function() {
              // CKEditor Standard
              $( 'textarea#ckeditor_full' ).ckeditor({

              });

              $('#title' ).on('keyup', function(){
                title = $(this).val();
                if(title.length > 0){
                  $('.bootstrap-admin-box-title' ).html(title);
                } else {
                  $('.bootstrap-admin-box-title' ).html('New Article');
                }
              });

              $('.bootstrap-admin-box-title' ).html('New Article');

              $('#Articles' ).on('change', function(){
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                  //window.location = url; // redirect
                  window.open(url, '_blank');
                }
                return false;

              });
            });
        </script>
    </body>
</html>